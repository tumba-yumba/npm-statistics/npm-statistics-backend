from datetime import datetime

import requests
from fastapi import APIRouter, HTTPException

from src.config import config
from src.schemas import StatusResponse, SuccessResponse

router = APIRouter()
last_update_time: datetime = datetime.now()


@router.get("/collector/status")
async def get_status():
    return StatusResponse(
        last_update_time=last_update_time, update_is_allowed=update_is_allowed()
    )


@router.post("/collector/update")
async def update() -> SuccessResponse:
    global last_update_time

    if not update_is_allowed():
        raise HTTPException(status_code=429, detail="update is not allowed")

    last_update_time = datetime.now()
    requests.post(config.collector_url + "/collect")
    return SuccessResponse()


def update_is_allowed() -> bool:
    global last_update_time

    return (datetime.now() - last_update_time).total_seconds() >= 3600
