from fastapi import APIRouter, Depends

import src.schemas as schemas
from src.internal.utils import get_db_pagination
from src.timescaledb.client import db

router = APIRouter()


@router.get("/projects")
async def get_projects(
        pag: schemas.PaginationQuery = Depends(),
        project_filter: schemas.ProjectFilterQuery = Depends(),
        order_query: schemas.OrderByQuery = Depends(),
) -> schemas.ProjectsListResponse:
    return schemas.ProjectsListResponse(
        items=[
            schemas.ProjectResponse(
                id=project.id,
                name=project.name,
                repository_url=project.repository_url,
                package=schemas.PackageResponse(
                    name=package.name,
                    version=package.version,
                ) if project_filter.package_name or project_filter.package_version else None
            )
            for project, package in db.get_projects(
                package_version=project_filter.package_version,
                package_name=project_filter.package_name,
                order_by=order_query.order_by,
                **get_db_pagination(pag.page, pag.per_page),
            )
        ],
        total_count=db.get_projects_count(
            package_version=project_filter.package_version,
            package_name=project_filter.package_name
        ),
    )


@router.get("/projects/{project_id}/packages")
async def get_packages_in_project(
        project_id: str,
        package_name: str = "",
        pag: schemas.PaginationQuery = Depends()
) -> schemas.PackagesListResponse:
    return schemas.PackagesListResponse(
        items=[
            schemas.PackageResponse(
                name=package.name,
                version=package.version,
            )
            for package in db.get_packages_by_project_id_and_name(
                project_id=project_id,
                package_name=package_name,
                **get_db_pagination(pag.page, pag.per_page)
            )
        ],
        total_count=db.get_packages_count_by_project_id(project_id, package_name),
    )


@router.get("/projects/{project_id}/history")
async def get_package_history_in_project(
        project_id: str,
        package_name: str,
        pag: schemas.PaginationQuery = Depends(),
) -> schemas.PackageVersionTimeStatListResponse:
    return schemas.PackageVersionTimeStatListResponse(
        items=[
            schemas.PackageVersionTimeStatResponse(
                time=time,
                package_version=package_version,
            )
            for (package_version, time) in db.get_package_version_history_by_project_id(
                project_id=project_id,
                package_name=package_name,
                **get_db_pagination(pag.page, pag.per_page),
            )
        ],
        total_count=db.get_package_version_history_count_by_project_id(project_id, package_name),
    )
