from fastapi import APIRouter, Depends
from itertools import groupby

import src.schemas as schemas
from src.internal.utils import get_db_pagination
from src.timescaledb.client import db

router = APIRouter()


@router.get("/packages")
async def get_packages_by_name(
        name: str = "", pag: schemas.PaginationQuery = Depends()
):
    return schemas.PackageNamesListResponse(
        items=[
            schemas.PackageNameResponse(
                name=package.name,
            )
            for package in db.get_packages_by_name(
                name, **get_db_pagination(pag.page, pag.per_page)
            )
        ],
        total_count=db.get_packages_count_by_name(name),
    )


@router.get("/packages/stats/projects_count")
async def get_packages_stats_projects_count(
        package_name: str,
        package_version: str = "",
) -> schemas.PackageStatsProjectCountListResponse:
    return schemas.PackageStatsProjectCountListResponse(
        stats=[
            schemas.PackageStatsProjectCountResponse(
                package_version=package_version,
                projects_count=projects_count,
            )
            for (package_version, projects_count) in db.get_packages_stats_projects_count(
                package_name=package_name,
                package_version=package_version,
            )
        ]
    )


@router.get("/packages/stats/projects_count/last_month")
async def get_packages_stats_projects_count_last_month(
    package_name: str,
) -> list[schemas.PackageStatsProjectsCountDayResponse]:
    result = []
    data = db.get_packages_stats_project_last_month(package_name=package_name)
    for time, records in groupby(data, lambda x: x[0]):
        stats = []
        for record in records:
            stats.append(schemas.PackageStatsProjectCountResponse(
                package_version=record[1],
                projects_count=record[2],
            ))
        result.append(schemas.PackageStatsProjectsCountDayResponse(
            time=time,
            stats=stats,
        ))
    return result
