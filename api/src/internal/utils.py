def get_db_pagination(page: int, per_page: int) -> dict:
    return {"limit": per_page, "offset": page * per_page}
