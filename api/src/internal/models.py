from enum import Enum
from typing import Optional

from pydantic import BaseModel


class Package(BaseModel):
    name: str
    version: Optional[str] = None
    description: Optional[str] = None


class Project(BaseModel):
    id: int
    name: str
    repository_url: Optional[str] = None


class OrderEnum(str, Enum):
    asc_name = "name"
    desc_name = "-name"
