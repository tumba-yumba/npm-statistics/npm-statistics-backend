from pydantic_settings import BaseSettings, SettingsConfigDict


class Config(BaseSettings):
    timescaledb_database: str = "timescaledb"
    timescaledb_user: str = "timescaledb"
    timescaledb_password: str = "timescaledb"
    timescaledb_host: str = "timescaledb"
    timescaledb_port: str = "5432"
    collector_url: str = "http://collector:8001"

    model_config = SettingsConfigDict(env_file=".env")


config = Config()  # type: ignore
