from contextlib import contextmanager
from datetime import datetime
from typing import Iterable, Tuple

import psycopg2
import psycopg2.pool
from psycopg2.extensions import AsIs

from src.config import config
from src.internal.models import OrderEnum, Package, Project
from src.timescaledb.utils import get_order_by_query, parse_semver_to_sql


class TimescaleDBClient:
    def __init__(self, user: str, password: str, host: str, port: str, db: str):
        self.conn_str = f"user={user} password={password} host={host} port={port} dbname={db}"
        self.dbpool = psycopg2.pool.ThreadedConnectionPool(1, 10, self.conn_str)

    @contextmanager
    def db_cursor(self):
        conn = self.dbpool.getconn()
        try:
            with conn.cursor() as cur:
                yield cur
                conn.commit()
        except psycopg2.Error:
            conn.rollback()
            raise
        finally:
            self.dbpool.putconn(conn)

    def get_packages_by_project_id_and_name(self, project_id: str, package_name: str, limit: int, offset: int) -> \
            Iterable[Package]:
        with self.db_cursor() as cur:
            cur.execute("""
                SELECT packages.name, packages.version
                FROM packages
                    JOIN projects ON projects.id = packages.project_id
                WHERE projects.id = %(project_id)s AND LOWER(packages.name) LIKE %(package_name)s
                LIMIT %(limit)s
                OFFSET %(offset)s
            """, {"project_id": project_id, "package_name": f"%{package_name.lower()}%", "limit": limit,
                  "offset": offset})
            return (
                Package(
                    name=record[0],
                    version=record[1],
                )
                for record in cur.fetchall()
            )

    def get_packages_count_by_project_id(self, project_id: str, package_name: str) -> int:
        with self.db_cursor() as cur:
            cur.execute("""
                SELECT count(*)
                FROM packages
                    JOIN projects ON projects.id = packages.project_id
                WHERE projects.id = %(project_id)s AND LOWER(packages.name) LIKE %(package_name)s
            """, {"project_id": project_id, "package_name": f"%{package_name.lower()}"})
            return cur.fetchone()[0]

    def get_projects(
            self,
            limit: int,
            offset: int,
            package_name: str = "",
            package_version: str = "",
            order_by: OrderEnum = None,
    ) -> Iterable:
        order_by_query = get_order_by_query(order_by)
        semver_operator = parse_semver_to_sql(package_version)
        with self.db_cursor() as cur:
            cur.execute("""
                WITH projects_with_package AS (
                    SELECT DISTINCT ON (projects.id) 
                        projects.id AS id, 
                        projects.name AS project_name, 
                        projects.repository_url AS repository_url,
                        packages.name AS package_name, 
                        packages.version AS package_version
                    FROM projects
                        JOIN packages ON projects.id = packages.project_id
                    WHERE 
                        (%(name)s = '' OR packages.name =  %(name)s) AND
                        (%(version)s = '' OR %(semver_operator)s)
                    ORDER BY projects.id, packages.time DESC
                )
                SELECT id, project_name, repository_url, package_name, package_version
                FROM projects_with_package
                ORDER BY %(order_by)s
                LIMIT %(limit)s
                OFFSET %(offset)s
            """, {
                "name": package_name,
                "version": package_version,
                "semver_operator": AsIs(semver_operator),
                "limit": limit,
                "offset": offset,
                "order_by": AsIs(order_by_query),
            })
            return (
                (Project(id=record[0], name=record[1], repository_url=record[2]),
                 Package(name=record[3], version=record[4]))
                for record in cur.fetchall()
            )

    def get_projects_count(self, package_name: str = "", package_version: str = "") -> int:
        semver_operator = parse_semver_to_sql(package_version)
        with self.db_cursor() as cur:
            cur.execute("""
                SELECT count(DISTINCT projects.id)
                FROM projects
                    JOIN packages ON projects.id = packages.project_id
                WHERE 
                    (%(name)s = '' OR packages.name =  %(name)s) AND
                    (%(version)s = '' OR %(semver_operator)s)
            """, {"name": package_name, "version": package_version, "semver_operator": AsIs(semver_operator)})
            return cur.fetchone()[0]

    def get_packages_by_name(self, name: str, limit: str, offset: int) -> Iterable[Package]:
        with self.db_cursor() as cur:
            cur.execute("""
                SELECT packages.name
                FROM packages
                WHERE LOWER(name) LIKE %(name)s
                GROUP BY packages.name
                LIMIT %(limit)s
                OFFSET %(offset)s
            """, {"name": f"%{name.lower()}%", "limit": limit, "offset": offset})
            return (
                Package(name=record[0])
                for record in cur.fetchall()
            )

    def get_packages_count_by_name(self, name: str) -> int:
        with self.db_cursor() as cur:
            cur.execute("""
                SELECT count(DISTINCT packages.name)
                FROM packages
                WHERE LOWER(name) LIKE %(name)s
            """, {"name": f"%{name.lower()}%"})
            return cur.fetchone()[0]

    def get_package_version_history_by_project_id(
            self,
            project_id: str,
            package_name: str,
            limit: int,
            offset: int
    ) -> Iterable[Tuple[str, datetime]]:
        with self.db_cursor() as cur:
            cur.execute("""
                SELECT packages.version, packages.time
                FROM packages
                    JOIN projects ON projects.id = packages.project_id
                WHERE packages.name = %(package_name)s AND packages.project_id = %(project_id)s
                ORDER BY packages.time DESC
                LIMIT %(limit)s
                OFFSET %(offset)s
            """, {"package_name": package_name, "project_id": project_id, "limit": limit, "offset": offset})
            return [
                (record[0], record[1])
                for record in cur.fetchall()
            ]

    def get_package_version_history_count_by_project_id(
            self,
            project_id: str,
            package_name: str,
    ) -> int:
        with self.db_cursor() as cur:
            cur.execute("""
                SELECT count(*)
                FROM packages
                    JOIN projects ON projects.id = packages.project_id
                WHERE packages.name = %(package_name)s AND packages.project_id = %(project_id)s
            """, {"package_name": package_name, "project_id": project_id})
            return cur.fetchone()[0]

    def get_packages_stats_projects_count(
            self,
            package_name: str,
            package_version: str,
    ) -> Iterable[Tuple[str, int]]:
        semver_operator = parse_semver_to_sql(package_version)
        with self.db_cursor() as cur:
            cur.execute("""
                WITH actual_package_versions AS (
                    SELECT DISTINCT ON (projects.id) projects.id as project_id, packages.version as package_version
                    FROM projects
                        JOIN packages ON projects.id = packages.project_id
                    WHERE 
                        packages.name = %(package_name)s AND
                        (%(package_version)s = '' OR %(semver_operator)s)
                    ORDER BY projects.id ASC, packages.time DESC
                )
                SELECT package_version, COUNT(project_id)
                FROM actual_package_versions
                GROUP BY package_version
            """, {
                "package_name": package_name,
                "package_version": package_version,
                "semver_operator": AsIs(semver_operator),
            })
            return [
                (record[0], record[1])
                for record in cur.fetchall()
            ]

    def get_packages_stats_project_last_month(self, package_name: str) -> Iterable[Tuple[datetime, str, int]]:
        with self.db_cursor() as cur:
            cur.execute("""
                WITH actual_package_versions AS (
                    SELECT DISTINCT ON (projects.id) 
                        projects.id as project_id, 
                        packages.version as package_version,
                        packages.time as package_time
                    FROM projects
                        JOIN packages ON projects.id = packages.project_id
                    WHERE 
                        packages.name = %(package_name)s AND
                        packages.time >= date_trunc('month', now())
                    ORDER BY projects.id ASC, packages.time DESC
                )
                SELECT 
                    time_bucket('1 day', package_time) as bucket_day, 
                    package_version, 
                    COUNT(project_id) as projects_count
                FROM actual_package_versions
                GROUP BY bucket_day, package_version
            """, {"package_name": package_name})
            return [
                (record[0], record[1], record[2])
                for record in cur.fetchall()
            ]


db = TimescaleDBClient(
    user=config.timescaledb_user,
    password=config.timescaledb_password,
    host=config.timescaledb_host,
    port=config.timescaledb_port,
    db=config.timescaledb_database,
)  # type: ignore
