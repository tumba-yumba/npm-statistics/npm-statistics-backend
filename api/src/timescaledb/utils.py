from fastapi import HTTPException

from src.internal.models import OrderEnum
from semantic_version import NpmSpec


def get_order_by_query(order_by: OrderEnum) -> str:
    if order_by == OrderEnum.asc_name:
        return "project_name ASC"
    return "project_name DESC"


def parse_semver_filter(semver_filter: str) -> (str, str):
    semver_filter = semver_filter.strip().replace(' ', '')

    operator = '='
    semver = semver_filter
    if not semver_filter:
        return operator, semver

    if semver_filter[:2] in ('<=', '>='):
        operator = semver_filter[:2]
        semver = semver_filter[2:]
    elif semver_filter[0] in ('<', '>', '='):
        operator = semver_filter[0]
        semver = semver_filter[1:]
    return operator, semver


def parse_range(range_clause) -> str:
    op = range_clause.operator
    if op == '==':
        op = '='
    return f"to_semver(packages.version) {op} to_semver('{range_clause.target}')"


def parse_clause(clause) -> str:
    clause_name = clause.__class__.__name__
    if clause_name == 'Range':
        return parse_range(clause)
    if clause_name == 'AllOf':
        return '(' + ' AND '.join(map(parse_clause, clause)) + ')'
    if clause_name == 'AnyOf':
        return '(' + ' OR '.join(map(parse_clause, clause)) + ')'
    raise RuntimeError(f'Unknown clause: {clause_name}')


def parse_semver_to_sql(semver_filter: str) -> str:
    try:
        parsed = NpmSpec(semver_filter)
    except ValueError as e:
        raise HTTPException(status_code=400, detail=f"Wrong filter: {str(e)}")
    return parse_clause(parsed.clause)
