from datetime import datetime
from typing import Optional

from pydantic import BaseModel

from src.internal.models import OrderEnum


class PaginationQuery(BaseModel):
    page: int = 0
    per_page: int = 100


class OrderByQuery(BaseModel):
    order_by: OrderEnum = OrderEnum.asc_name


class SuccessResponse(BaseModel):
    success: bool = True


class StatusResponse(BaseModel):
    last_update_time: datetime
    update_is_allowed: bool


class ProjectFilterQuery(BaseModel):
    package_version: str = ""
    package_name: str = ""


class PackageResponse(BaseModel):
    name: str
    version: str


class ProjectResponse(BaseModel):
    id: int
    name: str
    repository_url: str
    package: Optional[PackageResponse]


class ProjectsListResponse(BaseModel):
    items: list[ProjectResponse]
    total_count: int


class PackagesListResponse(BaseModel):
    items: list[PackageResponse]
    total_count: int


class PackageNameResponse(BaseModel):
    name: str


class PackageNamesListResponse(BaseModel):
    items: list[PackageNameResponse]
    total_count: int


class PackageVersionTimeStatResponse(BaseModel):
    time: datetime
    package_version: str


class PackageVersionTimeStatListResponse(BaseModel):
    items: list[PackageVersionTimeStatResponse]
    total_count: int


class PackageStatsProjectCountResponse(BaseModel):
    package_version: str
    projects_count: int


class PackageStatsProjectCountListResponse(BaseModel):
    stats: list[PackageStatsProjectCountResponse]


class PackageStatsProjectsCountDayResponse(BaseModel):
    time: datetime
    stats: list[PackageStatsProjectCountResponse]
