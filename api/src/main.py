from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from src.routers import collector, packages, projects

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(projects.router)
app.include_router(packages.router)
app.include_router(collector.router)
