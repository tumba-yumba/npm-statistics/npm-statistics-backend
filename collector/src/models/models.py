from dataclasses import dataclass


@dataclass(frozen=True)
class Package:
    name: str
    version: str


@dataclass
class Project:
    file_path: str
    name: str
    packages: list[Package]


@dataclass
class Repository:
    id: int
    url: str
    projects: list[Project]


@dataclass
class PackageFile:
    project_id: int
    file_path: str
    commit_sha: str


class PackageFilesList:
    def __init__(self, data: list[PackageFile]):
        self.data = data

    def get_package_file_commit_sha(
        self, project_id: int, file_path: str
    ) -> str | None:
        for package_file in self.data:
            if (
                package_file.project_id == project_id
                and package_file.file_path == file_path
            ):
                return package_file.commit_sha
        return None
