import yaml
from pyarn.lockfile import Lockfile

from src.models.models import Package, Project

YARN_V1_HEADER = 'yarn lockfile v1'
YARN_V2_EXCLUDE = {'__metadata'}


def parse(raw_lock: str) -> Project:
    if YARN_V1_HEADER in raw_lock:
        packages = parse_v1(raw_lock)
    else:
        packages = parse_v2(raw_lock)
    return Project(
        name="",
        packages=packages,
        file_path="",
    )


def parse_v1(raw_lock: str) -> list[Package]:
    lock_data = Lockfile.from_str(raw_lock).data
    packages = []
    for names, data in lock_data.items():
        name = names.split()[0]
        name = name[:name.rfind('@')]
        packages.append(Package(name=name, version=data['version']))
    return packages


def parse_v2(raw_lock: str) -> list[Package]:
    try:
        lock_data = yaml.safe_load(raw_lock)
    except yaml.YAMLError:
        return []

    packages = []
    for names, data in lock_data.items():
        if names in YARN_V2_EXCLUDE:
            continue
        name = names.split()[0]
        name = name[:name.rfind('@')]
        packages.append(Package(name=name, version=data['version']))
    return packages
