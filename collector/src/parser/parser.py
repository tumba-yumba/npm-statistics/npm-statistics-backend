from enum import Enum

from src.models.models import Project
from src.parser import npm, yarn


class Signature(Enum):
    npm = "package-lock.json"
    yarn = "yarn.lock"


def parse_project(raw_lock: str, signature: str) -> Project:
    match signature:
        case Signature.npm.value:
            return npm.parse(raw_lock)
        case Signature.yarn.value:
            return yarn.parse(raw_lock)
        case _:
            return []
