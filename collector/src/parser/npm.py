import json
from typing import Any

from src.models.models import Package, Project


def parse(raw_data: str) -> Project:
    try:
        data = json.loads(raw_data)
    except json.JSONDecodeError:
        return []

    try:
        project_name = data["name"]
    except KeyError:
        return []

    if "packages" in data:
        return parse_from_packages_field(data["packages"], project_name)
    if "dependencies" in data:
        return parse_from_packages_field(data["dependencies"], project_name)
    return []


def parse_from_packages_field(packages: dict[str, Any], project_name: str) -> Project:
    return Project(
        name=project_name,
        packages=[
            Package(
                name=clear_package_name(name),
                version=data.get("version"),
            )
            for name, data in packages.items()
            if name != ""
        ],
        file_path="",
    )


def parse_from_dependencies_field(
    dependencies: dict[str, Any], project_name: str
) -> Project:
    return Project(
        name=project_name,
        packages=[
            Package(
                name=clear_package_name(name),
                version=data.get("version"),
            )
            for name, data in dependencies.items()
        ],
        file_path="",
    )


def clear_package_name(package_name: str):
    if "node_modules/" in package_name:
        return package_name.replace("node_modules/", "")
    return package_name
