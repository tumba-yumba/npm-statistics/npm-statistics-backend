from datetime import datetime

from sqlalchemy import (Column, DateTime, ForeignKey, Integer, MetaData, Table,
                        Text, create_engine, select, UniqueConstraint)
from sqlalchemy.dialects.postgresql import insert
from src.models.models import PackageFile, Repository


class TimescaleDBClient:
    def __init__(self, database: str, user: str, password: str, host: str, port: str):
        self.connection_string = (
            f"timescaledb://{user}:{password}@{host}:{port}/{database}"
        )

    def init_scheme(self):
        self.engine = create_engine(self.connection_string)

        metadata = MetaData()
        metadata.bind = self.engine

        self.projects_table = Table(
            "projects",
            metadata,
            Column("id", Integer, primary_key=True, autoincrement=True),
            Column("file_path", Text),
            Column("name", Text),
            Column("repository_id", Integer),
            Column("repository_url", Text),

            UniqueConstraint('file_path', 'repository_id')
        )

        self.packages_table = Table(
            "packages",
            metadata,
            Column("time", DateTime(timezone=True), default=datetime.now()),
            Column("name", Text),
            Column("version", Text),
            Column("project_id", Integer, ForeignKey("projects.id")),
            timescaledb_hypertable={"time_column_name": "time"},
        )

        self.package_files_table = Table(
            "package_files",
            metadata,
            Column("project_id", Integer, primary_key=True),
            Column("file_path", Text, primary_key=True),
            Column("commit_sha", Text),
        )

        metadata.create_all(self.engine)

    def insert_packages(self, repositories_with_parsed_packages: list[Repository]):
        with self.engine.connect() as connection:
            for repository in repositories_with_parsed_packages:
                for project in repository.projects:
                    result = connection.execute(
                        insert(self.projects_table)
                        .values(
                            name=project.name,
                            file_path=project.file_path,
                            repository_id=repository.id,
                            repository_url=repository.url,
                        )
                        .on_conflict_do_update(
                            index_elements=[
                                self.projects_table.c.file_path,
                                self.projects_table.c.repository_id,
                            ],
                            set_=dict(name=project.name),
                        )
                        .returning(self.projects_table.c.id)
                    )

                    project_id = result.fetchone()[0]

                    connection.execute(
                        insert(self.packages_table),
                        [
                            {
                                "name": package.name,
                                "version": package.version,
                                "project_id": project_id,
                            }
                            for package in project.packages
                        ],
                    )
            connection.commit()

    def insert_or_update_package_files(self, package_files: list[PackageFile]):
        with self.engine.connect() as connection:
            for package_file in package_files:
                connection.execute(
                    insert(self.package_files_table)
                    .values(
                        project_id=package_file.project_id,
                        file_path=package_file.file_path,
                        commit_sha=package_file.commit_sha,
                    )
                    .on_conflict_do_update(
                        index_elements=[
                            self.package_files_table.c.project_id,
                            self.package_files_table.c.file_path,
                        ],
                        set_=dict(commit_sha=package_file.commit_sha),
                    )
                )
            connection.commit()

    def get_package_files(self) -> list[PackageFile]:
        with self.engine.connect() as connection:
            result = [
                PackageFile(
                    project_id=row.project_id,
                    file_path=row.file_path,
                    commit_sha=row.commit_sha,
                )
                for row in connection.execute(select(self.package_files_table))
            ]
            connection.commit()
        return result
