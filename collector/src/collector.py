import asyncio
import logging

from fastapi import FastAPI
from src.config.config import load_config
from src.gitlab.gitlab import GitlabClient
from src.timescaledb.timescaledb import TimescaleDBClient

logging.basicConfig(
    format="%(asctime)s %(levelname)-8s [%(pathname)s:%(lineno)d in function %(funcName)s] %(message)s",
    datefmt="%Y-%m-%d:%H:%M:%S",
)
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

app = FastAPI()


class Collector:
    def __init__(
        self, gitlab_client: GitlabClient, timescaledb_client: TimescaleDBClient
    ):
        self.gitlab_client = gitlab_client
        self.timescaledb_client = timescaledb_client

    def collect(self):
        try:
            existing_package_files = self.timescaledb_client.get_package_files()
            (
                repositories_with_parsed_packages,
                packages_files_to_update,
            ) = self.gitlab_client.get_repositories_with_parsed_packages(
                existing_package_files
            )
            self.timescaledb_client.insert_packages(repositories_with_parsed_packages)
            self.timescaledb_client.insert_or_update_package_files(
                packages_files_to_update
            )
        except Exception as e:
            logger.exception(e)


@app.post("/collect")
async def collect():
    logger.info("Start collector")

    config = load_config()

    gitlab_client = GitlabClient(
        config.gitlab.url,
        config.gitlab.token,
        config.gitlab.group_ids,
        config.gitlab.debug,
    )
    gitlab_client.auth()

    timescaledb_client = TimescaleDBClient(
        config.timescaledb.database,
        config.timescaledb.user,
        config.timescaledb.password,
        config.timescaledb.host,
        config.timescaledb.port,
    )
    timescaledb_client.init_scheme()

    collector = Collector(gitlab_client, timescaledb_client)

    loop = asyncio.get_running_loop()
    loop.run_in_executor(None, collector.collect)

    return {"success": "true"}
