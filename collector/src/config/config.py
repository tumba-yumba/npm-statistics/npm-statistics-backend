from dataclasses import dataclass
from os import environ


@dataclass
class GitlabConfig:
    url: str
    token: str
    group_ids: list[int]
    debug: bool


@dataclass
class TimescaleDBConfig:
    database: str
    user: str
    password: str
    host: str
    port: str


@dataclass
class Config:
    gitlab: GitlabConfig
    timescaledb: TimescaleDBConfig


def load_config() -> Config:
    gitlab_config = GitlabConfig(
        url=environ.get("GITLAB_URL", "https://gitlab.com"),
        token=environ.get("GITLAB_TOKEN", ""),
        group_ids=[int(group) for group in environ.get("GITLAB_GROUP_IDS", "").split(',')],
        debug=environ.get("GITLAB_DEBUG", False),
    )

    timescaledb_config = TimescaleDBConfig(
        database=environ.get("TIMESCALEDB_DATABASE", "timescaledb"),
        user=environ.get("TIMESCALEDB_USER", "timescaledb"),
        password=environ.get("TIMESCALEDB_PASSWORD", "timescaledb"),
        host=environ.get("TIMESCALEDB_HOST", "timescaledb"),
        port=environ.get("TIMESCALEDB_PORT", 5432),
    )

    return Config(gitlab=gitlab_config, timescaledb=timescaledb_config)
