import logging

import gitlab
import requests
from src.models.models import PackageFile, PackageFilesList, Repository
from src.parser.parser import Signature, parse_project

PACKAGE_SIGNATURES = [signature.value for signature in Signature]

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class GitlabClient:
    def __init__(self, url: str, token: str, group_ids: list[int], debug: bool):
        self.url = url
        self.token = token
        self.group_ids = group_ids
        self.debug = debug
        self.gitlab = None

    def auth(self):
        self.session = requests.Session()
        self.session.headers.update({'PRIVATE-TOKEN': self.token})
        self.gitlab = gitlab.Gitlab(
            url=self.url,
            private_token=self.token,
            session=self.session,
        )
        self.gitlab.auth()
        if self.debug:
            self.gitlab.enable_debug()

    def get_repositories_with_parsed_packages(
        self, existing_package_files: list[PackageFile]
    ) -> (list[Repository], list[PackageFile]):
        repositories = []
        package_files_to_update = []

        existing_package_files = PackageFilesList(existing_package_files)
        for group_id in self.group_ids:
            try:
                gitlab_projects = self.gitlab.groups.get(
                    group_id
                ).projects.list(
                    iterator=True,
                    include_subgroups=True,
                )
            except gitlab.GitlabError as e:
                logger.error(
                    f"Error while fetching projects list from group {group_id}: {e.error_message}"
                )
                return [], []

            logger.info(
                f"Collected {len(gitlab_projects)} projects from group {group_id}"
            )
            for gitlab_project in gitlab_projects:
                logger.info(f"Processing project {gitlab_project.name}")
                try:
                    gitlab_project = self.gitlab.projects.get(gitlab_project.id)
                except gitlab.GitlabError as e:
                    logger.error(
                        f"Error while fetching project {gitlab_project.name}: {e.error_message}"
                    )
                    continue

                logger.info(f"Fetching repository tree of project {gitlab_project.name}")
                try:
                    tree = gitlab_project.repository_tree(recursive=True, iterator=True)
                except gitlab.GitlabError as e:
                    logger.error(
                        f"Error while fetching repository tree for project {gitlab_project.name}: {e.error_message}"
                    )
                    continue

                repository = Repository(
                    id=gitlab_project.id, url=gitlab_project.web_url, projects=[]
                )

                for file in tree:
                    file_name = file.get("name")

                    if file_name in PACKAGE_SIGNATURES:
                        logger.info(
                            f"Found package file {file_name} in project {gitlab_project.name}"
                        )
                        file_path = file.get("path")
                        saved_commit_sha = (
                            existing_package_files.get_package_file_commit_sha(
                                gitlab_project.id, file_path
                            )
                        )

                        try:
                            last_commit_sha = self.get_package_file_last_commit_sha(
                                gitlab_project.id, file_path
                            )
                        except (
                            requests.exceptions.RequestException,
                            gitlab.GitlabError,
                        ) as e:
                            logger.error(
                                f"Error while fetching last commit sha of file {file_path}: {e}"
                            )
                            continue

                        if saved_commit_sha is None or saved_commit_sha != last_commit_sha:
                            logger.info(
                                f"Downloading content of package file {file_name} in project {gitlab_project.name}"
                            )
                            try:
                                raw_data = (
                                    self.gitlab.projects.get(gitlab_project.id)
                                    .files.get(file_path, ref=gitlab_project.default_branch)
                                    .decode()
                                )
                            except gitlab.GitlabError as e:
                                logger.error(
                                    f"Error while fetching content of file {file_path}: {e.error_message}"
                                )
                                continue

                            logger.info(
                                f"Start parsing package file {file_name} in project {gitlab_project.name}"
                            )

                            try:
                                raw_data = raw_data.decode()
                                parsed_project = parse_project(raw_data, file_name)
                                parsed_project.file_path = file_path
                                if not parsed_project.name:
                                    parsed_project.name = gitlab_project.name
                            except Exception as e:
                                logger.error(
                                    f"Error while parse file {file_name} in project {gitlab_project.name} : {str(e)}"
                                )
                                continue
                            repository.projects.append(parsed_project)
                            package_files_to_update.append(
                                PackageFile(
                                    project_id=gitlab_project.id,
                                    file_path=file_path,
                                    commit_sha=last_commit_sha,
                                )
                            )
                            logger.info(
                                f"Parsed {len(parsed_project.packages)} packages in package file {file_name} in project {gitlab_project.name}"
                            )

                repositories.append(repository)

                logger.info(f"Project {gitlab_project.name} processed")

        return repositories, package_files_to_update

    def get_package_file_last_commit_sha(self, project_id: int, path: str) -> str:
        data = self.session.get(
            f"{self.url}/api/v4/projects/{project_id}/repository/commits",
            params={"path": path},
        ).json()
        last_commit_sha = data[0]["id"]
        return last_commit_sha
